# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
*[#White] Programando ordenadores \nen los 80 y ahora
	*[#Pink] Sistemas antiguos
		* Sistema que ya no ha tenido continuidad
		* Es la especificación de un conjunto de pasos orientados \na la resolución de un problema
		* Ordenadores de la década de los \n80's y 90's
			* La programación en los 80's y 90's
				*_ La velocidad de los CPU se median en MHz
				*_ Para poder programar en diferentes maquinas, \nera necesario conocer la arquitectura de cada máquina
				*_ Limitaciones de recursos
				*_ Se programaba en Ensamblador
	*[#Pink] Sistemas actuales
		* Son los que tenemos en casa o el \nque se puede adquirir en alguna tienda
	*[#Pink] Diferencias
		* Las potencias de las maquinas actuales \nes mucho mayor que las antiguas
	*[#Pink] Lenguajes
		* Bajo nivel
			* Lo realizaba el propio programador			
			* Ensamblador				
				* Aprovechar bien los recursos
				* Control total

		* Alto nivel 
			* Actualmente lo realizan los compiladores\nde intérpretes
				* Java
					* ventajas
						*_ No se compila directamente a la máquina, \nsino que se ejecuta en una máquina virtual
	*[#Pink] Leyes
		* Moore
			* Cada 18 meses se pueden generar \nuna CPU el doble de rápido
				* Cada vez se cumple menos \npor limitaciones físicas
				* Programación gaseosa
		* Page
			* Cada 18 meses el software se \nvuelve el doble de lento
@endmindmap
```
# Hª de los algoritmos y de los lenguajes de programación
```plantuml
@startmindmap
*[#White] Historia de los algoritmos, máquinas y \nde los lenguajes de programación
	*[#Pink] Informática 
		* Manejo de procesadores de texto, \nhojas de cálculo y la ingeniería informática
		* Aporta maquinas rápidas que ejecutan \nlos algoritmos y notaciones precisas
	*[#Pink] Algoritmo
		* Es una lista bien definida, ordenada y finita de operaciones \nque permite hallar la solución a un problema dado un estado inicial y \nuna entrada a través de pasos sucesivos y bien definidos \nse llega a un estado final obteniendo una solución
			* importancia 	
				* radica en mostrar la manera que se lleva a \ncabo procesos y resolver mecánicamente problemas
				* reciben una entrada y la \ntransformar en una salida
				* para que se considere como algoritmo debe ser \nuna secuencia ordenada, finita y definida de instrucciones
				* Se puede seguir y predecir \nel comportamiento del algoritmo
			* Ejemplos:
				* Multiplicar números
				* Recetas de cocina
			* Historia 
				* Antigua Mesopotamia
					* Se emplearon algoritmos \npara describir cálculos
				* Siglo XVII
					* Aparecieron las primeras ayudas mecánicas para \nel cálculo en forma de calculadoras de sobremesa
				* Siglo XIX 
					* Se conciben las primeras \nmáquinas programables
				* Siglo XX 		
					* Los primeros ordenadores \ncomo en la actualidad
		* Se dividen en
			* Razonables
				* Son algoritmos cuyo tiempo de ejecución \ncrece despacio a medida que los \nproblemas se van haciendo más grandes 
					*_ Ejemplo: Multiplicar dos números de muchas cifras, \nse considera que es un problema razonable- polinomiales
			* No razonables
				* Se le llama exponenciales o super polinomiales \nson aquellos que se comportan muy mal
					*_ Ejemplo: Añadir un dato más al problema hace que se \nduplique el tiempo
	*[#Pink] Maquinas 
		* Las primeras computadoras de la historia \nla "maquina analítica" que era mecánica
	*[#Pink] Dispositivos 
		* Unidad de babbage que no es muy distinto a la CPU
			* Operaciones básicas
				*_ Suma
				*_ Resta
				*_ Multiplicación
				*_ División
	*[#Pink] Limites Teóricos
		* En los 30's se demostró que hay problemas \ndefinidos pero no admiten algoritmo
			* Problemas indecidibles
				*_ Saber si el programa terminara o no 
				*_ Saber cuánta memoria va a consumir
				*_ Cuanto tiempo tardara en ejecutarse 
				*_ Etc
	*[#Pink] P	
		* Conjunto de los problemas que se \npueden resolver en tiempo polimonia \nCon un algoritmo razonable
	*[#Pink] NP
		* Conjunto de problemas cuya solución \nse puede probar que es buena en tiempo polinomial	
	*[#Pink] Lenguajes de programación
		* Paradigmas o familias de lenguaje
			* 1959 FORTRAN
				*_ Primer lenguaje de Alto nivel
			* 1960 LISP
				*_ paradigma funcional
			* 1968 SIMULA
				*_ Lenguajes orientados a objetos
			* 1971 Prolog
				*_ Lenguaje a que computa a bases de fórmulas lógicas
			* Programas concurrentes \nProgramas paralelos 
				*_ Contiene muchas familias de lenguaje y \ndentro de cada una hay muchos lenguajes
		* Lenguajes de \nprogramacion actuales
			* Orientado a Objetos
				*_ 1995 Java	
				*_ 1980 c++
				*_ 1970 C
		* Lenguajes lógicos 
			* Para problemas con optimización
			* En la inteligencia artificial
		* Lenguajes funcionales 
			* Tiene capacidad para crear \nprocesos paralelos distribuidos
@endmindmap
```
# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
*[#White] La evolución de los lenguajes \ny paradigmas de programación
	*[#Pink] El ordenador
		* Creado para facilitar el trabajo intelectual 
	*[#Pink] Lenguajes de programación:
		* Medio de comunicación \nentre el hombre y la maquina
		* Siempre innovar para resolver problemas diversos y complejos, \nfacilitando la eficiencia de ejecución
		* Porque los necesitamos?: \nlos necesitamos para cambiar ese campo semántico
			*_ Codificado en lenguaje binario
			*_ Se usan Acrónimos en lenguaje ensamblador (Bajo nivel)
	*[#Pink] Paradigma:
		* Es una forma de aproximarse o abordar el \ndesarrollo de un programa para dar \nuna solución concreta
		* ¿Por qué aparecen?
			* Su creación se da por las fallas de la programación \nestructurada se crean otros paradigmas para corregir
		* Tipos
			* Funcional
				* Los programas sean correctos, \nnecesitamos comprobarlo y verificarlo
					* herramientas
						*_ funciones matemáticas
						*_ Uso de recursividad hasta que \nse cumpla cierta condición
				* Ejemplos de lenguajes:
					* ML
					* HOPE
					* HASKELL
					* ERLANG
			* Lógico
				* Expresiones lógicas 
					* Lenguaje de la lógica
					* Predicados lógicos que solo \npuedo devolver cierto o falso
						*_ Mecanismos de inferencia
					* Uso de recursividad
					* Ejemplo de lenguaje
						*_ Prolog
				* Lenguaje de la lógica		
	*[#Pink] Programación estructurada:
		* No tenemos que pensar en las estructuras de \nla memoria porque tenemos estructuras
			* Abstractas 
			* De control 
		* Diseño del software más modular
		* Consiste en analizar una función principal, \ndescomponer a funciones más sencillas de resolver
		* Ejemplos:
			* Basic
			* C
			* Pascal
			* Fortran
		* POO
			* Java
				* El cliente tiene la pretensión \nde cambiar sus necesidades
				* Permite algoritmos de diálogo \nentre los objetos
				* Los objetos nos dan una forma de \npensar más abstracta
	*[#Pink] Programación Concurrente
		* Es una manera de concebir los problemas que surgen y \nque pretende dar solución a multitud de usuarios accedan simultáneamente
		* Se programan políticas de sincronización y coordinación
			* Ejemplo de políticas:
				*_ Una cuenta vacía no puede sacar dinero 
				*_ Una cuenta que hace una transacción no \npuede realizar otra simultáneamente  
	*[#Pink] Programación distribuida
		* Comunicar entre los ordenadores
		* Programas pesados en estar en diversos ordenadores
			* Ejemplo: Proyecto SETI
				*_ Se distribuye en multitud de ordenadores \npor medio de internet para la analizar toda la información
	*[#Pink] Programación basada en \ncomponentes
		* Se puede ver como un componente es un \nconjunto de objetos que nos da una funcionalidad mayor
		* Permite la reutilización a la \nhora de construir nuevos programas
			* Ejemplo: Un motor de un coche \ncontiene piezas aún más pequeñas
	*[#Pink] Programación orientada \nen aspectos
		* Al implementar el esqueleto del programa, incorporamos \nnuevos aspectos hasta construir el sistema. \nSe agrega capas en base a la necesidad del programa
		* Todos los aspectos son separados
	*[#Pink] Programación orientada agente \nsoftware y los sistemas multiagentes
		* Los agentes softwares surgen dentro del \ncampo de la inteligencia artificial
		* Son aplicaciones informáticas con capacidad \nde decidir cómo deben actuar para alcanzar sus objetivos
		* Los Agentes son entidades autónomas que \nperciben su entorno, interfaz gráfica del usuario 
		* Tienen la necesidad de alcanzar sus objetivos
		* Multiagentes 
			* Revuelven grandes problemas complejos, \ndialogo, cooperación, coordinación y negociación
		* Ejemplo: El agente busca recomendaciones con \nbase en los gustos del perfil del cliente

@endmindmap




